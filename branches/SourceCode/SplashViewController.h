//
//  SplashViewController.h
//  iCarta
//
//  Created by Roberto Capelo on 30/03/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SplashViewController : UIViewController {

}
- (void)showSplash;
- (void)hideSplash;

@end
