//
//  Confirmation.h
//  Cardapio
//
//  Created by Roberto Capelo on 14/11/10.
//  Copyright 2010 Roberto Capelo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmationViewController: UIViewController{
	
		IBOutlet UITextField *newItemField;
		IBOutlet UITableView *toDoTableView;
		IBOutlet UIButton *addButton;
		
		NSMutableArray *toDoList;
}	

@property (nonatomic, retain) IBOutlet UIButton *Button;
@property (nonatomic,retain) IBOutlet UITextField *newItemField;
@property (nonatomic,retain) IBOutlet UITableView *toDoTableView;
@property (nonatomic,retain) IBOutlet UIButton *addButton;
@property (nonatomic,retain) IBOutlet NSMutableArray *toDoList;



-(IBAction) confirme_OnClick: (id) sender;
-(IBAction)addIt:(id)sender;

@end
