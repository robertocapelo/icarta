    //
//  VideoViewController.m
//  iCarta
//
//  Created by Roberto Capelo on 17/03/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import "VideoViewController.h"


@implementation VideoViewController


-(IBAction) back_OnClick: (id) sender{

    [player stop];
	
	
	// [self.view removeFromSuperView];
    //[player autorelease]; 
	
	[self.parentViewController dismissModalViewControllerAnimated:YES];
}	
	
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	//self.navigationController.navigationBarHidden = NO;
	
	NSLog(@"lendo....");
	NSString *url = [[NSBundle mainBundle] 
					 pathForResource:@"che" 
					 ofType:@"mp4"];
    player = 
	[[MPMoviePlayerController alloc] 
	 initWithContentURL:[NSURL fileURLWithPath:url]];
    
    [[NSNotificationCenter defaultCenter] 
	 addObserver:self
	 selector:@selector(movieFinishedCallback:)
	 name:MPMoviePlayerPlaybackDidFinishNotification
	 object:player];
    
    //---play partial screen---
    player.view.frame = CGRectMake(184, 200, 400, 300);
	//player.view.frame = self.view.bounds;
	//player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:player.view];

	
    //---play movie---
    [player play];
	
    [super viewDidLoad];
}

- (void) movieFinishedCallback:(NSNotification*) aNotification {
    player = [aNotification object];
    [[NSNotificationCenter defaultCenter] 
	 removeObserver:self
	 name:MPMoviePlayerPlaybackDidFinishNotification
	 object:player];
    [player stop];
	
	
	// [self.view removeFromSuperView];
    [player autorelease]; 
	
	[self.parentViewController dismissModalViewControllerAnimated:YES];
	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
