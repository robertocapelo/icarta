    //
//  OpcoesViewController.m
//  Cardapio
//
//  Created by Roberto Capelo on 07/11/10.
//  Copyright 2010 Roberto Capelo. All rights reserved.
//

#import "OpcoesViewController.h"
#import "CadastroViewController.h"

@implementation OpcoesViewController

@synthesize example1;
@synthesize videoViewController;

-(IBAction) goVideo_OnClick: (id) sender{
	[self presentModalViewController:videoViewController animated:TRUE];
	[videoViewController loadView];
}

-(IBAction) goBase_OnClick: (id) sender{
	[baseViewController init];
	[self.navigationController pushViewController:baseViewController animated:YES];
}

/* Criada tela via codigo */
-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
	
	CGRect labelFrame = CGRectMake( 0, 0, 768, 1004 );
	
	UIImageView* splashImage = [[UIImageView alloc] initWithFrame:labelFrame];
	[splashImage setImage:[UIImage imageNamed:@"LOUSA.png"]];
	[splashImage setBackgroundColor:[UIColor orangeColor]];
	
    [modalViewController.view addSubview: splashImage];
	[modalViewController.view setBackgroundColor: [UIColor yellowColor]];
	
	[self presentModalViewController:modalViewController animated:NO];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:2.0];
	
}

- (void)hideSplash{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

//- (void)viewDidLoad {
	
		//[indicador hidesWhenStopped];
		//[indicador stopAnimating];

		//self.view.transform = CGAffineTransformIdentity;
		//self.view.transform = CGAffineTransformMakeRotation((M_PI * (90) / 180.0)); 
		//self.view.bounds = CGRectMake(0.0, 0.0, 480, 320);
	
		//	[[self navigationController] setNavigationBarHidden:UIInterfaceOrientationIsLandscape(toInterfaceOrientation) animated:YES];
		//[[UIApplication sharedApplication] setStatusBarHidden:UIInterfaceOrientationIsLandscape(toInterfaceOrientation) animated:YES];
		//}

- (IBAction) createData{
    /*
    LoadDataPage *load = [[LoadDataPage new]autorelease];
    NSDictionary *map = [load listPage:1];
    NSEnumerator *enumerator = [map keyEnumerator];
    id key;
    while ( key = [enumerator nextObject] ) {
        printf( "---%s => %s\n",
               [[key description] cString],
               [[[map objectForKey: key] description] cString] );
    }
    */
    //NSLog(@"cria/insere e busca dados...sqlite3");
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"dadosx.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *dbpath;
    
    
//    Create table if not exist tbl_pagina (id_pagina INTEGER, nome_pagina TEXT, fundo_pagina TEXT); 
//    Create table if not exist tbl_item (id_item INTEGER, descricao_item TEXT, x_item TEXT, y_item TEXT, foto_item, pagina_item);

    
 //   if ([filemgr fileExistsAtPath: databasePath ] == NO){
        dbpath = [databasePath UTF8String];
        NSLog(@"DATABASE PATH: %@",databasePath);
        
        if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK)
        {
            char *errMsg;
            
            const char *sql_stmt = "CREATE TABLE if not exists [tbl_pagina] (pk_pagina integer PRIMARY KEY,st_nome text, st_fundo text)";
            
            if (sqlite3_exec(dadosDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK){
                NSLog(@"Failed to create table pagina");
            }

            sql_stmt = "CREATE TABLE if not exists [tbl_cardapio] (pk_cardapio integer PRIMARY KEY,img_capa text, img_background text, st_titulo text, st_descricao, fk_usuario )";
            
            if (sqlite3_exec(dadosDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK){
                NSLog(@"Failed to create table pagina");
            }

            sql_stmt = "CREATE TABLE if not exists [tbl_cardapio_pagina] (pk_cardapio_pagina integer PRIMARY KEY,fk_cardapio integer, fk_pagina integer )";
            
            if (sqlite3_exec(dadosDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK){
                NSLog(@"Failed to create table pagina");
            }
            
            sql_stmt = "CREATE TABLE if not exists [tbl_usuario_cardapio] (pk_usuario_cardapio integer PRIMARY KEY,fk_usuario integer, fk_cardapio integer, sincronizado boolean )";
            
            if (sqlite3_exec(dadosDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK){
                NSLog(@"Failed to create table pagina");
            }
            
            sql_stmt = "CREATE TABLE if not exists [tbl_usuario] (pk_usuario integer PRIMARY KEY,st_nome text, fk_cardapio integer, sincronizado boolean )";
            
            if (sqlite3_exec(dadosDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK){
                NSLog(@"Failed to create table pagina");
            }
            
            sql_stmt = "CREATE TABLE if not exists [tbl_item] (pk_item integer PRIMARY KEY,st_descricao text, x text, y text, icone text, fk_pagina integer, foto text, preco real )";
            
            if (sqlite3_exec(dadosDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK){
                NSLog(@"Failed to create table pagina");
            }
            

            
            
            sqlite3_close(dadosDB);
            
            
            
            
            
            
            /*
            //CRIAR DADOS INICIAIS
// PAGINA 1
            sqlite3_stmt    *statement;
            //1
            NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO PAGINA (ID, NOME, FUNDO, IDITEM) VALUES (%d, \"%@\", \"%@\", %d)", 1,@"PRIMEIRA", @"FUNDO",1];
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...1");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            //2
            insertSQL = [NSString stringWithFormat: @"INSERT INTO PAGINA (ID, NOME, FUNDO, IDITEM) VALUES (%d, \"%@\", \"%@\", %d)", 1,@"PRIMEIRA", @"FUNDO",2];
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt2 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt2, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...2");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            //3
            insertSQL = [NSString stringWithFormat: @"INSERT INTO PAGINA (ID, NOME, FUNDO, IDITEM) VALUES (%d, \"%@\", \"%@\", %d)", 1,@"PRIMEIRA", @"FUNDO",3];
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt3 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt3, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...3");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            //4
            insertSQL = [NSString stringWithFormat: @"INSERT INTO PAGINA (ID, NOME, FUNDO, IDITEM) VALUES (%d, \"%@\", \"%@\", %d)", 1,@"PRIMEIRA", @"FUNDO",4];
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt4 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt4, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...4");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            
            //PAGINA 2
            insertSQL = [NSString stringWithFormat: @"INSERT INTO PAGINA (ID, NOME, FUNDO, IDITEM) VALUES (%d, \"%@\", \"%@\", %d)", 2,@"PRIMEIRA", @"FUNDO",5];
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt5 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt5, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...5");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            
            
//ITENS DA PAGINA 1
            //1
            insertSQL = [NSString stringWithFormat: @"INSERT INTO ITEM (ID, DESC, IMAGEM, X, Y, FOTO) VALUES (%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", 1, @"PRIMEIRO ITEM", @"FOTO.png", @"120", @"700", @"ARROZ.jpg"];

            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt6 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt6, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...item..6");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            //2
            insertSQL = [NSString stringWithFormat: @"INSERT INTO ITEM (ID, DESC, IMAGEM, X, Y, FOTO) VALUES (%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", 2, @"SEGUNDO ITEM", @"FOTO.png", @"120", @"500", @"FEIJOADA.jpg"];
            
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt7 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt7, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...item..6");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            //3
            insertSQL = [NSString stringWithFormat: @"INSERT INTO ITEM (ID, DESC, IMAGEM, X, Y, FOTO) VALUES (%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", 3, @"TERCEIRO ITEM", @"FOTO.png", @"120", @"300", @"ARROZ.jpg"];
            
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt8 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt8, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...item..6");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            //4
            insertSQL = [NSString stringWithFormat: @"INSERT INTO ITEM (ID, DESC, IMAGEM, X, Y, FOTO) VALUES (%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", 4, @"QUARTO ITEM", @"FOTO.png", @"120", @"100", @"FEIJOADA.jpg"];
            
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt9 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt9, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...item..9");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            //5
            insertSQL = [NSString stringWithFormat: @"INSERT INTO ITEM (ID, DESC, IMAGEM, X, Y, FOTO) VALUES (%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", 5, @"PRIMEIRO ITEM", @"FOTO.png", @"300", @"400", @"FEIJOADA.jpg"];
            
            NSLog(@"sql: %@",insertSQL);
            
            const char *insert_stmt10 = [insertSQL UTF8String];
            
            sqlite3_prepare_v2(dadosDB, insert_stmt10, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"done...item..10");
            } else {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
            }
            sqlite3_finalize(statement);
            
             sqlite3_close(dadosDB);
             */
            
            
        } else {
            NSLog(@"Failed to open/create database");
        }
//    }
    
    [filemgr release];
    
    
   // sqlite3_stmt    *statement;
    
    //dbpath = [databasePath UTF8String];
    /*
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK)
    {
        
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO PAGINA (ID, NOME, FUNDO, IDITEM) VALUES (%d, \"%@\", \"%@\", %d)", 1,@"PRIMEIRA", 2];
        NSLog(@"sql: %@",insertSQL);
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dadosDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"done...");
        } else {
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
        }
        sqlite3_finalize(statement);
        sqlite3_close(dadosDB);
    }
    */
    /*
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK)
    {
        
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO ITEM (ID, DESC, IMAGEM, X, Y, FOTO) VALUES (%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", 2, @"SEGUNDO ITEM", @"imagem2.png", @"300", @"400", @"fotoprato2.png"];
        NSLog(@"sql: %@",insertSQL);
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dadosDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"done...");
        } else {
            NSLog(@"Failed to add ITEM");
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
        }
        sqlite3_finalize(statement);
        sqlite3_close(dadosDB);
    }
    */
    //CONSULTA...
    /*
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK){
        
        NSString *querySQL =@"SELECT IDITEM,DESC,IMAGEM,FUNDO FROM PAGINA, ITEM WHERE PAGINA.IDITEM=ITEM.ID AND PAGINA.ID=1";
        sqlite3_stmt* statement;
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(dadosDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *addressField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                NSString *phoneField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSString *fotoField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSString *fundoField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                
                NSLog(@"ID:%@ DESC:%@ IMG:%@ FUNDO:%@",addressField,phoneField,fotoField,fundoField);
                
                [addressField release];
                [phoneField release];
            }
            sqlite3_finalize(statement);
        }else{
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
        }
        sqlite3_close(dadosDB);
    }
    */
/*
    {
        int articlesCount = 0;
        if (sqlite3_open([self.dataBasePath UTF8String], &articlesDB) == SQLITE_OK) 
        {
            const char* sqlStatement = "SELECT COUNT(*) FROM ARTICLES";
            sqlite3_stmt* statement;
            
            if( sqlite3_prepare_v2(articlesDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK ) 
            {
                if( sqlite3_step(statement) == SQLITE_ROW )
                    articlesCount  = sqlite3_column_int(statement, 0); 
            }
            else
            {
                NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(articlesDB) );
            }
            
            // Finalize and close database.
            sqlite3_finalize(statement);
            sqlite3_close(articlesDB);
        }
        
        return articlesCount;
    }    
    
    
*/   
    
    
    [super viewDidLoad];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"OpcoesViewController ################...");
    [self createData];
	//self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(IBAction) showAPAE_OnClick: (id) sender{
	
	PDFExampleViewController *pdfView = [[PDFExampleViewController alloc]autorelease];
	[pdfView setFileName:@"APAE"];
	[pdfView init];
	viewController = pdfView;
	
	[self.navigationController pushViewController:viewController animated:YES];
	
}

-(IBAction) showFormulario_OnClick: (id) sender{
	
	OpcoesViewController *opcoesView = [[CadastroViewController alloc]autorelease];
	[opcoesView init];
	
	[self.navigationController pushViewController:opcoesView animated:YES];
}

- (IBAction) showBook_OnClick:(id) sender {
	
	viewController = [[[ExamplesViewController alloc]init]autorelease];
	[self.navigationController pushViewController:viewController animated:YES];
}

-(IBAction) showPageImage_OnClick: (id) sender{
	NSLog(@"pageImage load");
	
	ImageExampleViewController *pdfView = [[ImageExampleViewController alloc]autorelease];
	[pdfView init];
	viewController = pdfView;
	
	[self.navigationController pushViewController:viewController animated:YES];
}

-(IBAction) callWebService:(id)sender{
    
    example1 = [[ICartaWSICartaServiceExample alloc] init];
    [example1 run];
    
    [NSThread detachNewThreadSelector:@selector(verificador:) toTarget:self withObject:nil];
    
    NSLog(@"WS EXEC %@",[example1 status]);
    

    
}

-(void)verificador:(id)param{
    
    int x;

    while ([example1 status] == @"this is the new shit!"){
        x++;
        printf("Main thread says x is %i\n",x);  
        usleep(10);
    }
    
    NSLog(@"fim thread...");
    
    
    
    NSLog(@"FUCK...%@",[example1 sincronizadoRetorno]);
    
    
}

- (void)dealloc {
    [super dealloc];
}


@end
