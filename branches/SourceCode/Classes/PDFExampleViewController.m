//
//  PDFExampleViewController.m
//  Leaves
//
//

#import "PDFExampleViewController.h"
#import "Utilities.h"
#import "OpcoesViewController.h"
#import "ConfirmationViewController.h"

@implementation PDFExampleViewController

- (id)init {
    if (self = [super init]) {
		CFURLRef pdfURL;

			if (fileName == @"APAE"){
				pdfURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR( "APAE.pdf" ) , NULL, NULL);
			}else if (fileName == @"HISTORIA"){
				pdfURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR( "HISTORIA.pdf" ) , NULL, NULL);
			}			
		
		
			//CFURLRef pdfURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR( "cfString" ) , NULL, NULL);

		pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
		CFRelease(pdfURL);
		NSLog(@"card...");
		
		/*
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] 
												  initWithTitle:@"Adicionar" 
												  style:UIBarButtonItemStyleDone 
												  target:self 
												  action:@selector(handleButton:)] 
												 autorelease];
		
		*/
		
		
    }
    return self;
}

- (void) handleButton:(id)sender
{
	NSLog(@"testando evento botao");
	
	OpcoesViewController *opcoesView = [[ConfirmationViewController alloc]autorelease];
	[opcoesView init];
	
	[self.navigationController pushViewController:opcoesView animated:YES];

}

- (void) setFileName:(NSString*)name{
	fileName=name;
		//	[fileName initWithFormat:"%@",name];
}

	//-(NSString) fileName

- (void)dealloc {
	CGPDFDocumentRelease(pdf);
    [super dealloc];
}

- (void) displayPageNumber:(NSUInteger)pageNumber {
	
	self.navigationItem.title = [NSString stringWithFormat:
								 @"Página %u de %u", 
								 pageNumber, 
								 CGPDFDocumentGetNumberOfPages(pdf)];

	
	NSLog(@"displayPageNumber");
}

-(void) displayScreen{
	NSLog(@"displayScreen...");
	[self.parentViewController dismissModalViewControllerAnimated:YES];
		//[self presentModalViewController:pdf animated:NO];
	
}




#pragma mark  LeavesViewDelegate methods

- (void) leavesView:(LeavesView *)leavesView willShowImage:(NSUInteger)numero{
	NSLog(@"dentro do pdf....leavesView");
	
		//UIImage *image2 = [UIImage imageNamed:@"core.jpg"];
	
		//myImage = [[UIImageView alloc] initWithImage:image2];
	
		//[leavesView addSubview:myImage];
	
	//CHAMA TELA DE CONFIMACAO
	
	OpcoesViewController *opcoesView = [[ConfirmationViewController alloc]autorelease];
	[opcoesView init];
	
	[self.navigationController pushViewController:opcoesView animated:YES];
	
	
}


- (void) leavesView:(LeavesView *)leavesView willTurnToPageAtIndex:(NSUInteger)pageIndex {
	[self displayPageNumber:pageIndex + 1];
	myImage.hidden=true;
		//[myImage release];
	
}

#pragma mark LeavesViewDataSource methods



- (NSUInteger) numberOfPagesInLeavesView:(LeavesView*)leavesView {
	return CGPDFDocumentGetNumberOfPages(pdf);
}

- (void) showImage:(NSUInteger)index inContext:(CGContextRef)ctx {
//	NSLog(@"SHOWImage..");
	
		//UIImage *image2 = [UIImage imageNamed:@"core.jpg"];
	
		//CGRect imageRect2 = CGRectMake(10, 100, 150, 100);
		//	CGContextDrawImage(ctx, imageRect2, [image2 CGImage]);
}

- (void) renderPageAtIndex:(NSUInteger)index inContext:(CGContextRef)ctx {

	
	CGPDFPageRef page = CGPDFDocumentGetPage(pdf, index + 1);
	CGAffineTransform transform = aspectFit(CGPDFPageGetBoxRect(page, kCGPDFMediaBox),
											CGContextGetClipBoundingBox(ctx));
	CGContextConcatCTM(ctx, transform);
	CGContextDrawPDFPage(ctx, page);
	NSLog(@"renderpageAtIndex...%i",index);
}

#pragma mark UIViewController

- (void) viewDidLoad {
	[super viewDidLoad];
	leavesView.backgroundRendering = YES;
	[self displayPageNumber:1];
}

@end
