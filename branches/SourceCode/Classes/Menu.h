//
//  Menu.h
//  Leaves
//
//  Created by Roberto Capelo on 04/10/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Menu : UIViewController {
	IBOutlet UIView *displaySplashScreen;
}

-(void)displayScreen;

@end
