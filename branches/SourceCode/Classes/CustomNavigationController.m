    //
//  CustomNavigationController.m
//  Cardapio
//
//  Created by Roberto Capelo on 14/11/10.
//  Copyright 2010 Roberto Capelo. All rights reserved.
//

#import "CustomNavigationController.h"

@implementation CustomNavigationController

@synthesize target;
@synthesize action;

- (void) clearTargetAction 
{
	target = nil;
	action = NULL;
}

- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item 
{
	BOOL rv = TRUE;
	if (target && action)
	{
		rv = FALSE;
		objc_msgSend(target, action, self);
	} else
	{
		if ([super respondsToSelector:@selector(navigationBar:shouldPopItem:)])
		{
			rv = [(id <UINavigationBarDelegate>)super navigationBar:navigationBar shouldPopItem:item];
		}
	}
	
	return rv;
}

@end