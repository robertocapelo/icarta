//
//  OpcoesViewController.h
//  Cardapio
//
//  Created by Roberto Capelo on 07/11/10.
//  Copyright 2010 Roberto Capelo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExamplesViewController.h"
#import "PDFExampleViewController.h"
#import "ImageExampleViewController.h"
#import "VideoViewController.h"
#import "BaseViewController.h"

#import <sqlite3.h>
#import "LoadDataPage.h"
#import "ICartaWSICartaServiceExample.h"



@interface OpcoesViewController : UIViewController {
	IBOutlet UIView *displayMenu;

	IBOutlet NewLeavesViewController *viewController;
	IBOutlet PDFExampleViewController *pdfViewController;
	IBOutlet ImageExampleViewController *imageViewController;
	
	IBOutlet VideoViewController *videoViewController;
	IBOutlet BaseViewController *baseViewController;
	
	IBOutlet LeavesView *leaves;
    
    ICartaWSICartaServiceExample* example1;
    
    NSString *databasePath;
    sqlite3 *dadosDB;
}	

-(void)verificador:(id)param;

-(IBAction) createData;

@property (nonatomic,retain) IBOutlet ICartaWSICartaServiceExample* example1;

@property (nonatomic,retain) IBOutlet VideoViewController *videoViewController;

-(IBAction) goVideo_OnClick: (id) sender;

-(IBAction) goBase_OnClick: (id) sender;

-(IBAction) showBook_OnClick: (id) sender;

-(IBAction) showFormulario_OnClick: (id) sender;

-(IBAction) showAPAE_OnClick: (id) sender;

-(IBAction) showPageImage_OnClick: (id) sender;

-(IBAction) callWebService: (id) sender;

- (void)showSplash;
- (void)hideSplash;

@end
