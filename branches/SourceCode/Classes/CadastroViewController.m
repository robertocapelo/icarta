    //
//  CadastroViewController.m
//  Cardapio
//
//  Created by Roberto Capelo on 18/11/10.
//  Copyright 2010 Roberto Capelo. All rights reserved.
//

#import "CadastroViewController.h"


@implementation CadastroViewController

- (void)viewDidLoad {
	
	NSString *urlAddress = @"http://capelo.blog.com";
	NSURL *url = [NSURL URLWithString:urlAddress];
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
	[webDisplay loadRequest:requestObj];
	
	[super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
    [super dealloc];
}

@end
