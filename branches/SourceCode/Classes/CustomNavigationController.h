//
//  CustomNavigationController.h
//  Cardapio
//
//  Created by Roberto Capelo on 14/11/10.
//  Copyright 2010 Roberto Capelo. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomNavigationController : UINavigationController <UINavigationBarDelegate> 
{
	id target;
	SEL action;
}

@property(nonatomic) SEL action ;
@property(nonatomic, assign) id target ;

- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item ;
- (void)viewWillAppear:(BOOL)animated;
@end
