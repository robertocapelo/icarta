//
//  MinimalExampleViewController.h
//  Leaves
//
//  Created by Tom Brow on 4/20/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//

#import "NewLeavesViewController.h"


@interface ProceduralExampleViewController : NewLeavesViewController {
	IBOutlet UIButton *Button;
}

@property (nonatomic,retain) IBOutlet UIButton *Button;

@end
