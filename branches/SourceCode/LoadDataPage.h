//
//  LoadDataPage.h
//  iCarta
//
//  Created by Luis Saenz on 23/07/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface LoadDataPage : NSObject{
    
    NSString *databasePath;
    sqlite3 *dadosDB;
}


- (NSMutableDictionary*)listPage:(int)npagina;
- (NSInteger*)countPages;

@end