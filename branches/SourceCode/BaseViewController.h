//
//  BaseViewController.h
//  iCarta
//
//  Created by Roberto Capelo on 28/03/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageExampleViewController.h"
#import "NewLeavesViewController.h"

@interface BaseViewController : NewLeavesViewController {
	IBOutlet NewLeavesViewController *viewController;
	
	IBOutlet UITextField *prato1;
	IBOutlet UITextField *prato2;
	IBOutlet UITextField *prato3;
	IBOutlet UITextField *prato4;
}
@property (nonatomic,retain) IBOutlet UITextField *prato1;
@property (nonatomic,retain) IBOutlet UITextField *prato2;
@property (nonatomic,retain) IBOutlet UITextField *prato3;
@property (nonatomic,retain) IBOutlet UITextField *prato4;

- (IBAction) back_onClick: (id) sender;
- (IBAction) showPagePratosPrincipais_OnClick: (id) sender;

@end
