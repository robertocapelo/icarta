//
//  Utilities.m
//  Leaves
//
//  Created by Tom Brow on 4/19/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//

CGAffineTransform aspectFit(CGRect innerRect, CGRect outerRect) {
	CGFloat scaleFactor = MIN(outerRect.size.width+180, outerRect.size.height/(innerRect.size.height-100));
	CGAffineTransform scale = CGAffineTransformMakeScale(scaleFactor, scaleFactor);
	CGRect scaledInnerRect = CGRectApplyAffineTransform(innerRect, scale);
		//	CGAffineTransform translation = 
		//	CGAffineTransformMakeTranslation((outerRect.size.width - scaledInnerRect.size.width) / 1 - scaledInnerRect.origin.x, 
		//								 (outerRect.size.height - scaledInnerRect.size.height) / 1 - scaledInnerRect.origin.y);

	CGAffineTransform translation = 
	CGAffineTransformMakeTranslation((outerRect.size.width - scaledInnerRect.size.width) , 
									 (outerRect.size.height - scaledInnerRect.size.height));
	
	return CGAffineTransformConcat(scale, translation);
}
