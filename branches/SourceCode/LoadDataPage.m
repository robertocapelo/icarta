//
//  LoadDataPage.m
//  iCarta
//
//  Created by Luis Saenz on 23/07/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import "LoadDataPage.h"


//Create table if not exist tbl_pagina (id_pagina INTEGER, nome_pagina TEXT, fundo_pagina TEXT); 
//Create table if not exist tbl_item (id_item INTEGER, descricao_item TEXT, x_item TEXT, y_item TEXT, foto_item, pagina_item);


@implementation LoadDataPage

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
- (NSInteger*)countPages{
    //return 2;
    NSString *docsDir;
    NSArray *dirPaths;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"dadosx.db"]];
    
    //NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *dbpath;
    
    dbpath = [databasePath UTF8String];
    NSLog(@"DATABASE PATH: %@",databasePath);
    
    NSMutableDictionary *mutable;
    int count=0;
    
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK){
        
        NSString *querySQL = @"SELECT * FROM tbl_pagina GROUP BY id_pagina";
        
        sqlite3_stmt* statement;
        
        const char *query_stmt = [querySQL UTF8String];
        //NSDictionary *dictionary = [[NSDictionary alloc]init];
        mutable = [[NSMutableDictionary alloc] init];
        
        if (sqlite3_prepare_v2(dadosDB, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            
            while (sqlite3_step(statement) == SQLITE_ROW){
                count++;
            }
        }
    }
    return count;
    
}

- (NSMutableDictionary*)listPage:(int)npagina{
    //NSLog(@"listPage...%i",npagina);
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"dadosx.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *dbpath;

    dbpath = [databasePath UTF8String];
    NSLog(@"DATABASE PATH: %@",databasePath);
    
    NSMutableDictionary *mutable;
    
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK){
        
        //NSString *querySQL = [NSString stringWithFormat:@"SELECT IDITEM,DESC,IMAGEM,FUNDO FROM PAGINA, ITEM WHERE PAGINA.IDITEM=ITEM.ID AND PAGINA.ID=%d",npagina];
        
        //NSString *querySQL = [NSString stringWithFormat:@"SELECT ID,NOME,IDITEM,FUNDO FROM PAGINA WHERE PAGINA.ID=%d",npagina];
        
        //NSString *querySQL = [NSString stringWithFormat:@"SELECT id_pagina,nome_pagina,fundo_pagina FROM tbl_pagina WHERE id_pagina=%d",npagina];
        
        NSString *querySQL = [NSString stringWithFormat:@"SELECT id_pagina, nome_pagina, fundo_pagina, id_item, descricao_item, x_item, y_item,foto_item, imagem_item FROM tbl_pagina, tbl_item WHERE id_pagina = pagina_item and id_pagina ==%d",npagina];
        
        NSLog(@"query:%@",querySQL);
        sqlite3_stmt* statement;
        
        const char *query_stmt = [querySQL UTF8String];
        //NSDictionary *dictionary = [[NSDictionary alloc]init];
        mutable = [[NSMutableDictionary alloc] init];
        
        if (sqlite3_prepare_v2(dadosDB, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            
            int count=0;
            
            while (sqlite3_step(statement) == SQLITE_ROW){
                count++;
                NSMutableArray *myArray = [NSMutableArray array];
                
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)] ];
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)] ];
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)] ];
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)] ];
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)] ];
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)] ];
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)] ];
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)] ];
                [myArray addObject: [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)] ];

                /*
                NSString *addressField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                [myArray addObject:addressField];
                NSString *phoneField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                [myArray addObject:phoneField];
                NSString *fotoField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                [myArray addObject:fotoField];
                NSString *fundoField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                [myArray addObject:fundoField];
                 
                NSLog(@"%i - ID:%@ DESC:%@ IMG:%@ FUNDO:%@",count,addressField,phoneField,fotoField,fundoField);
                */
                [mutable setValue:myArray forKey:[NSString stringWithFormat:@"%i",count]];
                
                //[addressField release];
                //[phoneField release];
            }
            sqlite3_finalize(statement);
        }else{
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
        }
        sqlite3_close(dadosDB);
    }

    
    [filemgr release];
    
    return mutable;
}


@end
