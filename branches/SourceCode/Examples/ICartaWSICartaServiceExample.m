
#import "ICartaWSICartaServiceExample.h"
#import "ICartaWSICartaService.h"

@implementation ICartaWSICartaServiceExample

- (void)run {
	// Create the service
	ICartaWSICartaService *service = [ICartaWSICartaService service];
	service.logging = YES;
	// service.username = @"username";
	// service.password = @"password";
	
	[service getSincronizado:self action:@selector(getSincronizadoHandler:)];

	// Returns NSString*. 
	//[service geraImg:self action:@selector(geraImgHandler:)];

	// Returns NSString*. 
	//[service getFundo:self action:@selector(getFundoHandler:)];

	// Returns NSString*. 
	//[service getFundo:self action:@selector(getFundoHandler:) pagina: 1];

    

}

- (void) getSincronizadoHandler: (id) value {
    
	// Handle errors
	if([value isKindOfClass:[NSError class]]) {
		NSLog(@"ERRO DO WS: %@", value);
		return;
	}
    
	// Handle faults
	if([value isKindOfClass:[SoapFault class]]) {
		NSLog(@"SINCRONIZADO -> %@", value);
		return;
	}				
    
    
	// Do something with the NSString* result
    NSString* result = (NSString*)value;
	NSLog(@"geraImg returned the value...: %@", result);
    NSLog(@"RETORNADO DO WS: %@", result);
}


// Handle the response from geraImg.
		
- (void) geraImgHandler: (id) value {

	// Handle errors
	if([value isKindOfClass:[NSError class]]) {
		NSLog(@"RETORNADO DO WS: %@", value);
		return;
	}

	// Handle faults
	if([value isKindOfClass:[SoapFault class]]) {
		NSLog(@"%@", value);
		return;
	}				
			

	// Do something with the NSString* result
		NSString* result = (NSString*)value;
	NSLog(@"geraImg returned the value...: %@", result);
    NSLog(@"RETORNADO DO WS: %@", result);
}
	

// Handle the response from getFundo.
		
- (void) getFundoHandlerx: (id) value {

	// Handle errors
	if([value isKindOfClass:[NSError class]]) {
		NSLog(@"%@", value);
		return;
	}

	// Handle faults
	if([value isKindOfClass:[SoapFault class]]) {
		NSLog(@"%@", value);
		return;
	}				
			

	// Do something with the NSString* result

	NSLog(@"getFundo returned the value: %@", (NSString*)value);
			
}
	

// Handle the response from getFundo.
		
- (void) getFundoHandler: (id) value {

	// Handle errors
	if([value isKindOfClass:[NSError class]]) {
		NSLog(@"%@", value);
		return;
	}

	// Handle faults
	if([value isKindOfClass:[SoapFault class]]) {
		NSLog(@"%@", value);
		return;
	}				
    
    NSString* result = (NSString*)value;
    //atualizar DB
    
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"dadosx.db"]];
    
    //NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *dbpath;
    
    //   if ([filemgr fileExistsAtPath: databasePath ] == NO){
    dbpath = [databasePath UTF8String];
    NSLog(@"DATABASE PATH: %@",databasePath);
    
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK){
        
        sqlite3_stmt    *statement;
        //1
        //SELECT * FROM tbl_pagina
        
        NSString *updateSQL = [NSString stringWithFormat: @"UPDATE tbl_pagina SET fundo_pagina=\"%@\" WHERE id_pagina=1", result];
        
        //NSString *updateSQL = [NSString stringWithFormat: @"UPDATE PAGINA SET FUNDO=\"%@\" WHERE ID=1", result];
        NSLog(@"sql update: %@",updateSQL);
        
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dadosDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"done...1");
        } else {
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(dadosDB) );
        }
        sqlite3_finalize(statement);

        sqlite3_close(dadosDB);
    }
		
	NSLog(@" XX  getFundo returned the value: %@", result);
			
}
	

@end
		