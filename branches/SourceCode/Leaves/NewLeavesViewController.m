    //
//  NewLeavesViewController.m
//  iCarta
//
//  Created by Roberto Capelo on 16/03/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import "NewLeavesViewController.h"


@implementation NewLeavesViewController

@synthesize popupView;

- (id)init {
	
	NSLog(@"leavesViewController");
    if (self = [super init]) {
		leavesView = [[LeavesView alloc] initWithFrame:CGRectZero];
    }
	
    return self;
}

- (void)dealloc {
	[leavesView release];
    [super dealloc];
}

#pragma mark LeavesViewDataSource methods

- (NSUInteger) numberOfPagesInLeavesView:(LeavesView*)leavesView {
	return 0;
}

- (void) renderPageAtIndex:(NSUInteger)index inContext:(CGContextRef)ctx {
	
}

#pragma mark  UIViewController methods

- (void)loadView {
	NSLog(@"loadView-LeavesViewController");
	
	[super loadView];
	leavesView.frame = self.view.bounds;
	leavesView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	[self.view addSubview:leavesView];
}

- (void) viewDidLoad {
	NSLog(@"loadView-viewDidLoad");
	[super viewDidLoad];
	leavesView.dataSource = self;
	leavesView.delegate = self;
	[leavesView reloadData];
}



@end
