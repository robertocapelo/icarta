//
//  NewLeavesViewController.h
//  iCarta
//
//  Created by Roberto Capelo on 16/03/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeavesView.h"

@interface NewLeavesViewController : UIViewController <LeavesViewDataSource, LeavesViewDelegate> {
	LeavesView *leavesView;
}

@property (nonatomic,retain) UIView *popupView;

@end
