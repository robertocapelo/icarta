#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class ICartaService_ArrayOfArrayOf_xsd_string;
@class ICartaService_ArrayOf_xsd_int;
@interface ICartaService_ArrayOfArrayOf_xsd_string : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ICartaService_ArrayOfArrayOf_xsd_string *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ICartaService_ArrayOf_xsd_int : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ICartaService_ArrayOf_xsd_int *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import ".h"
#import "ICartaService.h"
@class ICartaSoapBinding;
@interface ICartaService : NSObject {
	
}
+ (ICartaSoapBinding *)ICartaSoapBinding;
@end
@class ICartaSoapBindingResponse;
@class ICartaSoapBindingOperation;
@protocol ICartaSoapBindingResponseDelegate <NSObject>
- (void) operation:(ICartaSoapBindingOperation *)operation completedWithResponse:(ICartaSoapBindingResponse *)response;
@end
@interface ICartaSoapBinding : NSObject <ICartaSoapBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
	NSString *authUsername;
	NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(ICartaSoapBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (ICartaSoapBindingResponse *)getFundoUsing;
- (void)getFundoAsyncUsing delegate:(id<ICartaSoapBindingResponseDelegate>)responseDelegate;
- (ICartaSoapBindingResponse *)geraImgUsing;
- (void)geraImgAsyncUsing delegate:(id<ICartaSoapBindingResponseDelegate>)responseDelegate;
- (ICartaSoapBindingResponse *)getSincronizadoUsing;
- (void)getSincronizadoAsyncUsing delegate:(id<ICartaSoapBindingResponseDelegate>)responseDelegate;
- (ICartaSoapBindingResponse *)getPaginasUsingIds:(ICartaService_ArrayOf_xsd_int *)aIds ;
- (void)getPaginasAsyncUsingIds:(ICartaService_ArrayOf_xsd_int *)aIds  delegate:(id<ICartaSoapBindingResponseDelegate>)responseDelegate;
@end
@interface ICartaSoapBindingOperation : NSOperation {
	ICartaSoapBinding *binding;
	ICartaSoapBindingResponse *response;
	id<ICartaSoapBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) ICartaSoapBinding *binding;
@property (readonly) ICartaSoapBindingResponse *response;
@property (nonatomic, assign) id<ICartaSoapBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(ICartaSoapBinding *)aBinding delegate:(id<ICartaSoapBindingResponseDelegate>)aDelegate;
@end
@interface ICartaSoapBinding_getFundo : ICartaSoapBindingOperation {
}
- (id)initWithBinding:(ICartaSoapBinding *)aBinding delegate:(id<ICartaSoapBindingResponseDelegate>)aDelegate
;
@end
@interface ICartaSoapBinding_geraImg : ICartaSoapBindingOperation {
}
- (id)initWithBinding:(ICartaSoapBinding *)aBinding delegate:(id<ICartaSoapBindingResponseDelegate>)aDelegate
;
@end
@interface ICartaSoapBinding_getSincronizado : ICartaSoapBindingOperation {
}
- (id)initWithBinding:(ICartaSoapBinding *)aBinding delegate:(id<ICartaSoapBindingResponseDelegate>)aDelegate
;
@end
@interface ICartaSoapBinding_getPaginas : ICartaSoapBindingOperation {
	ICartaService_ArrayOf_xsd_int * ids;
}
@property (retain) ICartaService_ArrayOf_xsd_int * ids;
- (id)initWithBinding:(ICartaSoapBinding *)aBinding delegate:(id<ICartaSoapBindingResponseDelegate>)aDelegate
	ids:(ICartaService_ArrayOf_xsd_int *)aIds
;
@end
@interface ICartaSoapBinding_envelope : NSObject {
}
+ (ICartaSoapBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface ICartaSoapBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
