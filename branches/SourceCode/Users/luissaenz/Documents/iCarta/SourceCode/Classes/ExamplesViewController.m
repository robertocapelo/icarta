//
//  ExamplesViewController.m
//  Leaves
//
//  Created by Tom Brow on 4/20/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//

#import "ExamplesViewController.h"
#import "PDFExampleViewController.h"
#import "ImageExampleViewController.h"
#import "ProceduralExampleViewController.h"
#import "CustomNavigationController.h"

	//#import "Apresentation.h"

enum {PRINCIPAL, ENTRADA, CEBICHES, NUM_EXAMPLES};

@implementation ExamplesViewController

	//@synthesize tableView;


- (id)init {
		if ((self = [super initWithStyle:UITableViewStyleGrouped])) {

		}
	
	//self.view.backgroundColor = 
	//[UIColor colorWithPatternImage:[UIImage imageNamed:@"capacardapio.jpg"]]; 
	
	
	
	//NSLog(@"<><><><>");
	return self;
}


/*
- (void) loadView{
	if (self.nibName)
	{
		[super loadView];
		NSAssert(tableView != nil, @"NIB file did not set tableView property.");
		return;
	}
	
		//	UIButton *button = [[UIButton alloc]init ];
	

	
	UITableView *newTableView =
	[[[UITableView alloc]
	  initWithFrame:CGRectZero
	  style:UITableViewStyleGrouped]
	 autorelease];
	
		//newTableView.backgroundColor = 
		//	[UIColor colorWithPatternImage:[UIImage imageNamed:@"capacardapio.jpg"]]; 
		//newTableView.backgroundColor = [UIColor clearColor];
	
		//self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"capacardapio.jpg"]]; 
	
	
		//		self.navigationController.navigationItem.title=@"teste";

	
		//self.navigationItem.title = [NSString stringWithFormat:@"xxXxx"];
		//self.navigationItem.leftBarButtonItem.title=@"outro";
	
	
	
	
		self.view = newTableView;
		self.tableView = newTableView;
	
	NSLog(@"<---[loadTableView]--->");
}
*/
//- (UITableView *)tableView
//{
//	return tableView;
//}
//- (void)setTableView:(UITableView *)newTableView
//{
//	[tableView release];
//	tableView = [newTableView retain];
//	[tableView setDelegate:self];
//	[tableView setDataSource:self];
//}


- (void)dealloc {
	//[tableView release];
    [super dealloc];
}

#pragma mark UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return NUM_EXAMPLES;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
	switch (indexPath.row) {
		case PRINCIPAL: cell.textLabel.text = @"Entrada"; break;
		case ENTRADA: cell.textLabel.text = @"Principal"; break;
		case CEBICHES: cell.textLabel.text = @"Cebiches"; break;
				//case PROCEDURAL: cell.textLabel.text = @"Procedural example"; break;
		default: cell.textLabel.text = @"";
	}    
    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UIViewController *viewController;
		//Apresentation *viewController;
		//Apresentation *apresentation;
	PDFExampleViewController *pdfView;
	//ImageExampleViewController *imageView;
		//Apresentation *apresentation = [[[Apresentation alloc] init]autorelease];
	
	switch (indexPath.row) {
		case PRINCIPAL: 
				//			viewController = [[[PDFExampleViewController alloc] init] autorelease];
				//			[self.navigationController pushViewController:viewController animated:YES];
				//			break;
			pdfView = [[PDFExampleViewController alloc]autorelease];
			[pdfView setFileName:@"PRINCIPAL"];
			[pdfView init];
			viewController = pdfView;
			
			
			
			
			[self.navigationController pushViewController:viewController animated:YES];
			break;
		
		case ENTRADA: 
			pdfView = [[PDFExampleViewController alloc]autorelease];
			[pdfView setFileName:@"ENTRADA"];
			[pdfView init];
			viewController = pdfView;
			
			[self.navigationController pushViewController:viewController animated:YES];
			break;
			
		case CEBICHES:
			/*
			pdfView = [[PDFExampleViewController alloc]autorelease];
			[pdfView setFileName:@"CEBICHES"];
			[pdfView init];
			viewController = pdfView;
			
			[self.navigationController pushViewController:viewController animated:YES];
			break;
			 */
				//	PDFExampleViewController *pdf = [[[PDFExampleViewController alloc]init]autorelease];
				
				
				//[pdfview fileName:"@teste.pdf"];
				 
				 
				//pdf.fileNumber=1;
				//viewController=pdf;
				//[self.navigationController pushViewController:viewController animated:YES];
				//				viewController = [[[Apresentation alloc] init] autorelease];
				
				//viewController.view=displayApresentation;
				//viewController.view= apresentation.displayApresentation;
				//			viewController.view=apresent
				//UIViewController *viewController = [[[Apresentation alloc] init] autorelease];
				//			Apresentation *apres = [[[Apresentation alloc] init] autorelease];
				//viewController = [[[ProceduralExampleViewController alloc] init] autorelease]; 
				//viewController.view=apres;
				//viewController = [[[UIViewController alloc] init] autorelease];
			
				//imageView = [[[ImageExampleViewController alloc] init] autorelease]; 
				//viewController = imageView;
				//[self.navigationController pushViewController:viewController animated:YES];
				
			//pdfView = [[PDFExampleViewController alloc]autorelease];
			//[pdfView setFileName:@"CARDAPIO"];
			//[pdfView init];
			//viewController = pdfView;
			
			viewController = [[[ImageExampleViewController alloc] init] autorelease]; 
			
			[self.navigationController pushViewController:viewController animated:YES];
			
			
			break;
				//case PROCEDURAL:
				//viewController = [[[ProceduralExampleViewController alloc] init] autorelease]; 
				//break;
		default: 
			viewController = [[[UIViewController alloc] init] autorelease];
			
		
			
	} 
	
	
	
}


@end

