    //
//  OpcoesViewController.m
//  Cardapio
//
//  Created by Roberto Capelo on 07/11/10.
//  Copyright 2010 Roberto Capelo. All rights reserved.
//

#import "OpcoesViewController.h"
#import "CadastroViewController.h"

@implementation OpcoesViewController

@synthesize videoViewController;

-(IBAction) goVideo_OnClick: (id) sender{
	[self presentModalViewController:videoViewController animated:TRUE];
	[videoViewController loadView];
}

-(IBAction) goBase_OnClick: (id) sender{
	[baseViewController init];
	[self.navigationController pushViewController:baseViewController animated:YES];
}

/* Criada tela via codigo */
-(void)showSplash
{
    UIViewController *modalViewController = [[UIViewController alloc] init];
	
	CGRect labelFrame = CGRectMake( 0, 0, 768, 1004 );
	
	UIImageView* splashImage = [[UIImageView alloc] initWithFrame:labelFrame];
	[splashImage setImage:[UIImage imageNamed:@"LOUSA.png"]];
	[splashImage setBackgroundColor:[UIColor orangeColor]];
	
    [modalViewController.view addSubview: splashImage];
	[modalViewController.view setBackgroundColor: [UIColor yellowColor]];
	
	[self presentModalViewController:modalViewController animated:NO];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:2.0];
	
}

- (void)hideSplash{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

//- (void)viewDidLoad {
	
		//[indicador hidesWhenStopped];
		//[indicador stopAnimating];

		//self.view.transform = CGAffineTransformIdentity;
		//self.view.transform = CGAffineTransformMakeRotation((M_PI * (90) / 180.0)); 
		//self.view.bounds = CGRectMake(0.0, 0.0, 480, 320);
	
		//	[[self navigationController] setNavigationBarHidden:UIInterfaceOrientationIsLandscape(toInterfaceOrientation) animated:YES];
		//[[UIApplication sharedApplication] setStatusBarHidden:UIInterfaceOrientationIsLandscape(toInterfaceOrientation) animated:YES];
		//}

- (IBAction) createData{
    
    NSLog(@"cria/insere e busca dados...sqlite3");
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"dados1.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *dbpath;
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        dbpath = [databasePath UTF8String];
        NSLog(@"DATABASE PATH: %@",databasePath);
        
        if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK)
        {
            char *errMsg;
           
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS PAGINAS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, IDITEM INTEGER)";
            
            if (sqlite3_exec(dadosDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table paginas");
            }
            
            sql_stmt = "CREATE TABLE ITEM (ID INTEGER, DESC TEXT, IMAGEM TEXT, X TEXT, Y TEXT, FOTO TEXT)";
            
            if (sqlite3_exec(dadosDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table item");
                
            }  
            
            sqlite3_close(dadosDB);
            
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
    
    [filemgr release];
    
    
    sqlite3_stmt    *statement;
    
    dbpath = [databasePath UTF8String];
    /*
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK)
    {
        
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO PAGINAS (NAME, IDITEM) VALUES (\"%@\", %d)", @"PRIMEIRA", 1];
        NSLog(@"sql: %@",insertSQL);
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dadosDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"done...");
        } else {
            NSLog(@"Failed to add PAGINA");
        }
        sqlite3_finalize(statement);
        sqlite3_close(dadosDB);
    }
    */
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK)
    {
        
        NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO ITEM (ID, DESC, IMAGEM, X, Y, FOTO) VALUES (%d, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", 2, @"Terceiro ITEM", @"imagem3.png", @"300", @"400", @"fotoprato3.png"];
        NSLog(@"sql: %@",insertSQL);
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dadosDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"done...");
        } else {
            NSLog(@"Failed to add ITEM");
        }
        sqlite3_finalize(statement);
        sqlite3_close(dadosDB);
    }

    //CONSULTA...
    
    
    if (sqlite3_open(dbpath, &dadosDB) == SQLITE_OK)
    {
        NSString *querySQL =@"SELECT * FROM ITEM LEFT OUTER JOIN PAGINAS ON ITEM.ID = PAGINAS.IDITEM";
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(dadosDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *addressField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                NSLog(@"database....: %@",addressField);
                NSString *phoneField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSLog(@"database....: %@",phoneField);
                NSString *fotoField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSLog(@"database....: %@",fotoField);
                
                [addressField release];
                [phoneField release];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(dadosDB);
    }

    
    
    
    
    [super viewDidLoad];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"OpcoesViewController ################...");
    [self createData];
	//self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(IBAction) showAPAE_OnClick: (id) sender{
	
	PDFExampleViewController *pdfView = [[PDFExampleViewController alloc]autorelease];
	[pdfView setFileName:@"APAE"];
	[pdfView init];
	viewController = pdfView;
	
	[self.navigationController pushViewController:viewController animated:YES];
	
}

-(IBAction) showFormulario_OnClick: (id) sender{
	
	OpcoesViewController *opcoesView = [[CadastroViewController alloc]autorelease];
	[opcoesView init];
	
	[self.navigationController pushViewController:opcoesView animated:YES];
}

- (IBAction) showBook_OnClick:(id) sender {
	
	viewController = [[[ExamplesViewController alloc]init]autorelease];
	[self.navigationController pushViewController:viewController animated:YES];
}

-(IBAction) showPageImage_OnClick: (id) sender{
	NSLog(@"pageImage load");
	
	ImageExampleViewController *pdfView = [[ImageExampleViewController alloc]autorelease];
	[pdfView init];
	viewController = pdfView;
	
	[self.navigationController pushViewController:viewController animated:YES];
}

- (void)dealloc {
    [super dealloc];
}


@end
