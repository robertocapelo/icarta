//
//  LeavesAppDelegate.h
//  Leaves
//
//  Created by Tom Brow on 4/18/10.
//  Copyright Tom Brow 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OpcoesViewController;


@interface LeavesAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	OpcoesViewController *viewController;

}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet OpcoesViewController *viewController;

@end

