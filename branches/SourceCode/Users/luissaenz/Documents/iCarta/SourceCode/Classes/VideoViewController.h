//
//  VideoViewController.h
//  iCarta
//
//  Created by Roberto Capelo on 17/03/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface VideoViewController : UIViewController {
	MPMoviePlayerController *player;
}

- (void) loadView;
-(IBAction) back_OnClick: (id) sender;
@end
