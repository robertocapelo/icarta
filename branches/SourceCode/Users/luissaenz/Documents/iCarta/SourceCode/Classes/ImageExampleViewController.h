//
//  ExampleViewController.h
//  Leaves
//
//  Created by Tom Brow on 4/18/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//

#import "NewLeavesViewController.h"

@interface ImageExampleViewController : NewLeavesViewController {
	NSArray *map;
	UIImageView *myImage;
}

@property (nonatomic,retain) IBOutlet UIImageView *myImage;

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

@end
