    //
//  Confirmation.m
//  Cardapio
//
//  Created by Roberto Capelo on 14/11/10.
//  Copyright 2010 Roberto Capelo. All rights reserved.
//

#import "ConfirmationViewController.h"
	//#import "ExamplesViewController.h"

@implementation ConfirmationViewController

@synthesize Button;
@synthesize newItemField;
@synthesize toDoTableView;
@synthesize addButton;
@synthesize toDoList;

- (id)init
{
	[super init];
	if (self) {
        toDoList = [[NSMutableArray alloc] init];
    }
	
	return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"Returning num sections");
    return [toDoList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"Returning num rows");
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Trying to return cell ");
	
    static NSString *CellIdentifier = @"Cell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }
	

		//	NSString *item = [toDoList objectAtIndex:inde];
	
	
	
		//cell.textLabel.text=[toDoList objectAtIndex:indexPath.row];

    return cell;
}


- (IBAction)addIt:(id)sender{
	
	NSString *newToDo = newItemField.text;
	
		// Check for non-zero
	if ([newToDo length] == 0) {
		NSLog(@"New to-do was empty.");
		return;
	}
	
	NSLog(@"---->%@", newToDo);
	
	[toDoList addObject:newToDo];
	[toDoTableView reloadData];	        // Forces the table to reload the data view
										//[newItemField setStringValue:@""];      // Clears the text box after adding a new item
	
	NSLog(@"********%i", [toDoList count]);
}

- (int)numberOfRowsInTableView:(UITableView *)tv
{
	return [toDoList count];
}


- (IBAction) confirme_OnClick:(id) sender {
	NSLog(@"[confirmado]");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
    [super dealloc];
}


@end
