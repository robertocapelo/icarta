//
//  ExampleViewController.m
//  Leaves
//
//  Created by Tom Brow on 4/18/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//

#import "ImageExampleViewController.h"
#import "Utilities.h"

@implementation ImageExampleViewController


@synthesize myImage;



- (id)init {
    if (self = [super init]) {
		/*
		images = [[NSArray alloc] initWithObjects:
				  [UIImage imageNamed:@"fundo.png"],
				  [UIImage imageNamed:@"fundo.png"],
				  [UIImage imageNamed:@"fundo.png"],			  
				  [UIImage imageNamed:@"fundo.png"],
				  [UIImage imageNamed:@"fundo.png"],
				  nil];
		*/
		
		//map = [[NSArray alloc] initWithObjects:
		//	   @"L.png",@"K.png",@"J.png",@"H.png",@"G.png",nil];
		
		
		
   }
    return self;
}

- (void)dealloc {
	//[images release];
    [super dealloc];
}

#pragma mark LeavesViewDataSource methods

- (NSUInteger) numberOfPagesInLeavesView:(LeavesView*)leavesView {
	//return images.count;
	NSString *path = [[NSBundle mainBundle] pathForResource:@"map" ofType:@"plist"]; 
	NSDictionary *dict=[NSDictionary dictionaryWithContentsOfFile:path];

	return [dict count];
	
	
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	NSLog(@"clique....######");
	
	
	
}

- (void) showImage:(NSUInteger)index inContext:(CGContextRef)ctx {
	NSLog(@"SHOWImage..");
	
	// Carrega imagens configuradas de acordo com a pagina
	
	//if (index==2){
	//	UIImage *image2 = [UIImage imageNamed:@"logolamar2.jpg"];
	//	CGRect imageRect2 = CGRectMake(325, 630, 150, 100);
	//	CGContextDrawImage(ctx, imageRect2, [image2 CGImage]);
	//}
	
	
	//[self.parentViewController dismissModalViewControllerAnimated:YES];
	
	
	self.navigationController.navigationBarHidden = NO;
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	self.navigationController.navigationBar.backItem.title = @"Voltar";
	
	
	
	NSString *path = [[NSBundle mainBundle] pathForResource:@"map" ofType:@"plist"]; 
	NSDictionary *dict=[NSDictionary dictionaryWithContentsOfFile:path];

	NSDictionary *itens=[dict valueForKey:[NSString stringWithFormat:@"%d",index]];
	
	//NSLog(@"....:%@", [itens valueForKey:@"pagina"]);
	
	//[images addObject:[UIImage imageNamed:[itens valueForKey:@"pagina"]]];
	//[images addObject:@"pagina"];
	
	
	int contaImagem=0;
	
	NSDictionary *detalhes;
	
	do{
        
        detalhes= [itens valueForKey:[NSString stringWithFormat:@"item%d",contaImagem ]];
        if (detalhes != NULL){
            NSLog(@"immmm:%@",detalhes);
	
            UIImage *image2 = [UIImage imageNamed:[detalhes valueForKey:@"imagem"]];
	
            int x = [[detalhes valueForKey:@"x"]intValue];
            int y = [[detalhes valueForKey:@"y"]intValue];
		
            CGRect imageRect2;
            NSLog(@"==>> %d",y);
        
            imageRect2 = CGRectMake(x, 950-y, 150, 100);
            
        
            CGContextDrawImage(ctx, imageRect2, [image2 CGImage]);
            contaImagem++;
		}
	}while( detalhes!=nil);
	
	
	//NSLog(@"To load: %@",[map objectAtIndex:index]);
	
	
	//popupView.frame = CGRectMake(190, 45, 294, 469);
	//popupView.hidden=FALSE;
	
	
	//[self.view addSubview:popupView];
	
	
	
	
}

- (void) renderPageAtIndex:(NSUInteger)index inContext:(CGContextRef)ctx {
	
	// UIImage *image = [images objectAtIndex:index];
	// UIImage *image2 = [UIImage imageNamed:@"core.jpg"];
	 
	
	NSString *path = [[NSBundle mainBundle] pathForResource:@"map" ofType:@"plist"]; 
	NSDictionary *dict=[NSDictionary dictionaryWithContentsOfFile:path];
	
	NSDictionary *itens=[dict valueForKey:[NSString stringWithFormat:@"%d",index]];
	
	NSLog(@"BACKGROUND....:%@", [itens valueForKey:@"pagina"]);
	
	UIImage *image = [UIImage imageNamed:[itens valueForKey:@"pagina"]];
	
	 
	CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
	CGAffineTransform transform = aspectFit(imageRect,
	CGContextGetClipBoundingBox(ctx));
	CGContextConcatCTM(ctx, transform);
	CGContextDrawImage(ctx, imageRect, [image CGImage]);
	 
	
	
	
	
	 //if (index==3) {
	//	 CGRect imageRect2 = CGRectMake(100, 100, 200, 200);
	 
		// myImage = [[UIImageView alloc] initWithImage:image2];
	 
		// leavesView.userInteractionEnabled = YES;
	 
		// CGContextDrawImage(ctx, imageRect2, [image2 CGImage]);
	 
		// [leavesView addSubview:myImage]; 
	//	 myImage.hidden=NO;
	 
	 //}else{
	//	 NSLog(@"remove botao");
	//	 myImage.hidden=YES;
	// }
	
	NSLog(@"index:%i",index);
}

@end
