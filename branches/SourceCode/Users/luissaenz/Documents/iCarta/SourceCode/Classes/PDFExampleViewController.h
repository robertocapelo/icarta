//
//  PDFExampleViewController.h
//  Leaves
//
//  Created by Tom Brow on 4/19/10.
//  Copyright 2010 Tom Brow. All rights reserved.
//

#import "NewLeavesViewController.h"

@interface PDFExampleViewController : NewLeavesViewController {
	CGPDFDocumentRef pdf;
	NSString* fileName;
	UIImageView *myImage;
	UIButton *btn;
}


- (void) setFileName:(NSString*)name;
@end
