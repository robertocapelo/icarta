    //
//  BaseViewController.m
//  iCarta
//
//  Created by Roberto Capelo on 28/03/11.
//  Copyright 2011 Roberto Capelo. All rights reserved.
//

#import "BaseViewController.h"


@implementation BaseViewController

@synthesize prato1;
@synthesize prato2;
@synthesize prato3;
@synthesize prato4;

- (id)init {
    if (self == [super init]) {
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[prato1 setText:[defaults objectForKey:@"prato1_preference"]];
		[prato2 setText:[defaults objectForKey:@"prato2_preference"]];
		[prato3 setText:[defaults objectForKey:@"prato3_preference"]];
		[prato4 setText:[defaults objectForKey:@"prato4_preference"]];
		
		
    }
    return self;
}

- (void)viewDidLoad {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[prato1 setText:[defaults objectForKey:@"prato1_preference"]];
	[prato2 setText:[defaults objectForKey:@"prato2_preference"]];
	[prato3 setText:[defaults objectForKey:@"prato3_preference"]];
	[prato4 setText:[defaults objectForKey:@"prato4_preference"]];
	
    [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction) showPagePratosPrincipais_OnClick: (id) sender{
	
	NSLog(@"lendo pratos principais");
	ImageExampleViewController *pdfView = [[ImageExampleViewController alloc]autorelease];
	[pdfView init];
	viewController = pdfView;
	
	[self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction) back_onClick: (id) sender{
		[self.parentViewController dismissModalViewControllerAnimated:YES];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
    [super dealloc];
}


@end
