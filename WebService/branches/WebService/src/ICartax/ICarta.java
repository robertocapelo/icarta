package ICartax;

/**
 *  Classe de execu��o que roda os comandos interpretados como ws
 *  
 * @author Capelo
 *
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ICarta {

	/* teste nova query */

	public String[][] getSincronizado() {

		Connection con = null;
		Statement st = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			con = DriverManager
					.getConnection(
							"jdbc:sqlserver://coredobr.dynalias.net:1433;DatabaseName=LeGourmet",
							"icarta", "matilha1");
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet rs = null;

		System.out.println("conectado...");

		String[][] pegaValor = new String[30][6];

		try {
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			String query = "SELECT pk_pagina, pk_item, pk_usuario, pk_usuario_cardapio, pk_cardapio, pk_cardapio_pagina "
					+ "FROM tbl_pagina, tbl_item, tbl_usuario, tbl_usuario_cardapio,tbl_cardapio, tbl_cardapio_pagina "
					+ "WHERE pk_usuario = tbl_usuario_cardapio.fk_usuario "
					+ "AND pk_pagina = tbl_item.fk_pagina "
					+ "AND pk_cardapio = tbl_usuario_cardapio.fk_cardapio "
					+ "AND pk_usuario = tbl_usuario_cardapio.fk_usuario "
					+ "AND pk_pagina = tbl_cardapio_pagina.fk_pagina "
					+ "AND pk_cardapio = tbl_cardapio_pagina.fk_cardapio "
					+ "AND sincronizado = 1 ";
			System.out.println(query);
			rs = st.executeQuery(query);

			rs.last();
			int total = rs.getRow();
			pegaValor = new String[total][6];
			rs.first();

			int contador = -1;
			do {
				contador = contador + 1;
				pegaValor[contador][0] = rs.getString(1);
				pegaValor[contador][1] = rs.getString(2);
				pegaValor[contador][2] = rs.getString(3);
				pegaValor[contador][3] = rs.getString(4);
				pegaValor[contador][4] = rs.getString(5);
				pegaValor[contador][5] = rs.getString(6);
			} while (rs.next());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return (pegaValor);
	}

	public String[][] getPaginas(String ids) {

		if (ids == null) {
			String[][] valores = new String[1][3];
			valores[0][0] = "Sem parametros";
			valores[0][1] = "Sem parametros";
			valores[0][2] = "Sem parametros";
			return valores;
		}

		Connection con = null;
		Statement st = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String[][] valores = new String[30][3];
		try {
			con = DriverManager
					.getConnection(
							"jdbc:sqlserver://coredobr.dynalias.net:1433;DatabaseName=LeGourmet",
							"icarta", "matilha1");
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet rs = null;

		System.out.println("conectado...");

		try {
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			String query = "SELECT pk_pagina, st_nome, st_fundo from tbl_pagina where pk_pagina in ("
					+ ids + ")";
			System.out.println(query);

			rs = st.executeQuery(query);

			rs.last();
			int total = rs.getRow();
			valores = new String[total][3];
			rs.first();

			int cont = -1;
			do {
				cont = cont + 1;
				valores[cont][0] = rs.getString(1);
				valores[cont][1] = rs.getString(2);
				valores[cont][2] = rs.getString(3);

			} while (rs.next());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return (valores);
	}

	public String[][] getItens(String ids) {

		if (ids == null) {
			String[][] valores = new String[1][9];
			valores[0][0] = "Sem parametros";
			valores[0][1] = "Sem parametros";
			valores[0][2] = "Sem parametros";
			valores[0][3] = "Sem parametros";
			valores[0][4] = "Sem parametros";
			valores[0][5] = "Sem parametros";
			valores[0][6] = "Sem parametros";
			valores[0][7] = "Sem parametros";
			valores[0][8] = "Sem parametros";
			return valores;

		}

		Connection con = null;
		Statement st = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String[][] valores = new String[30][9];
		try {
			con = DriverManager
					.getConnection(
							"jdbc:sqlserver://coredobr.dynalias.net:1433;DatabaseName=LeGourmet",
							"icarta", "matilha1");
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet rs = null;

		System.out.println("conectado...");

		try {
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			String query = "SELECT pk_item, st_descricao, x, y, icone, fk_pagina, foto, preco from tbl_item where pk_item in("
					+ ids + ")";
			System.out.println(query);

			rs = st.executeQuery(query);

			rs.last();
			int total = rs.getRow();
			valores = new String[total][8];
			rs.first();

			int cont = -1;
			do {
				cont = cont + 1;
				valores[cont][0] = rs.getString(1);
				valores[cont][1] = rs.getString(2);
				valores[cont][2] = rs.getString(3);
				valores[cont][3] = rs.getString(4);
				valores[cont][4] = rs.getString(5);
				valores[cont][5] = rs.getString(6);
				valores[cont][6] = rs.getString(7);
				valores[cont][7] = rs.getString(8);
				// valores[cont][8] = rs.getString(9);
				// valores[cont][9] = rs.getString(9);
			} while (rs.next());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return (valores);
	}

	public String[][] getUsuarioCardapio(String ids) {

		if (ids == null) {
			String[][] valores = new String[1][4];
			valores[0][0] = "Sem parametros";
			valores[0][1] = "Sem parametros";
			valores[0][2] = "Sem parametros";
			valores[0][3] = "Sem parametros";
			return valores;

		}

		Connection con = null;
		Statement st = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String[][] valores = new String[30][4];
		try {
			con = DriverManager
					.getConnection(
							"jdbc:sqlserver://coredobr.dynalias.net:1433;DatabaseName=LeGourmet",
							"icarta", "matilha1");
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet rs = null;

		System.out.println("conectado...");

		try {
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			String query = "SELECT pk_usuario_cardapio, fk_usuario, fk_cardapio, sincronizado from tbl_usuario_cardapio where pk_usuario_cardapio in("
					+ ids + ")";
			System.out.println(query);

			rs = st.executeQuery(query);

			rs.last();
			int total = rs.getRow();
			valores = new String[total][4];
			rs.first();

			int cont = -1;
			do {
				cont = cont + 1;
				valores[cont][0] = rs.getString(1);
				valores[cont][1] = rs.getString(2);
				valores[cont][2] = rs.getString(3);
				valores[cont][3] = rs.getString(4);
			} while (rs.next());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return (valores);
	}

	public String[][] getCardapio(String ids) {

		if (ids == null) {
			String[][] valores = new String[1][7];
			valores[0][0] = "Sem parametros";
			valores[0][1] = "Sem parametros";
			valores[0][2] = "Sem parametros";
			valores[0][3] = "Sem parametros";
			valores[0][4] = "Sem parametros";
			valores[0][5] = "Sem parametros";
			valores[0][6] = "Sem parametros";

			return valores;

		}

		Connection con = null;
		Statement st = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String[][] valores = new String[30][7];
		try {
			con = DriverManager
					.getConnection(
							"jdbc:sqlserver://coredobr.dynalias.net:1433;DatabaseName=LeGourmet",
							"icarta", "matilha1");
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet rs = null;

		System.out.println("conectado...");

		try {
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			String query = "SELECT pk_cardapio, img_capa, img_background, st_titulo, st_descricao, bl_ativo, fk_usuario from tbl_cardapio where pk_cardapio in("
					+ ids + ")";
			System.out.println(query);

			rs = st.executeQuery(query);

			rs.last();
			int total = rs.getRow();
			valores = new String[total][7];
			rs.first();

			int cont = -1;
			do {
				cont = cont + 1;
				valores[cont][0] = rs.getString(1);
				valores[cont][1] = rs.getString(2);
				valores[cont][2] = rs.getString(3);
				valores[cont][3] = rs.getString(4);
				valores[cont][4] = rs.getString(5);
				valores[cont][5] = rs.getString(6);
				valores[cont][6] = rs.getString(7);
			} while (rs.next());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return (valores);
	}

	public String[][] getCardapioPagina(String ids) {

		if (ids == null) {
			String[][] valores = new String[1][3];
			valores[0][0] = "Sem parametros";
			valores[0][1] = "Sem parametros";
			valores[0][2] = "Sem parametros";
			return valores;
		}

		Connection con = null;
		Statement st = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String[][] valores = new String[30][3];
		try {
			con = DriverManager
					.getConnection(
							"jdbc:sqlserver://coredobr.dynalias.net:1433;DatabaseName=LeGourmet",
							"icarta", "matilha1");
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet rs = null;

		System.out.println("conectado...");

		try {
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			String query = "SELECT pk_cardapio_pagina, fk_cardapio, fk_pagina from tbl_cardapio_pagina where pk_cardapio_pagina in("
					+ ids + ")";
			System.out.println(query);

			rs = st.executeQuery(query);

			rs.last();
			int total = rs.getRow();
			valores = new String[total][3];
			rs.first();

			int cont = -1;
			do {
				cont = cont + 1;
				valores[cont][0] = rs.getString(1);
				valores[cont][1] = rs.getString(2);
				valores[cont][2] = rs.getString(3);
				} while (rs.next());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return (valores);
	}

	public String getFundo(int pagina) {
		String paginaDB = "vazio.png";

		Connection con = null;
		Statement st = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			con = DriverManager
					.getConnection(
							"jdbc:sqlserver://coredobr.dynalias.net:1433;DatabaseName=LeGourmet",
							"icarta", "matilha1");
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet rs = null;

		System.out.println("conectado...");

		try {
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery("select * from tbl_paginas where id=" + pagina);
		} catch (SQLException e) {

		}

		if (rs != null) {
			try {
				while (rs.next()) {
					System.out.println(rs.getString(2));
					paginaDB = rs.getString(2);
				}
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		try {
			con.close();
			st.close();

		} catch (SQLException e) {

		}

		return (paginaDB);
	}

	public String getFundo() {

		return ("fundosemparametroxxxx.png");
	}

	public String geraImg() {

		return ("gerado.png");
	}
}
